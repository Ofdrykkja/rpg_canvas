function UNIT(X, Y, sprite_src, parent_layer, action_layer, username){
    this.X = X || this.constructor.prototype.X;
    this.Y = Y || this.constructor.prototype.Y;
    this.username = username || this.constructor.prototype.username;
    this.sprite_src = sprite_src;
    this.parent_layer = parent_layer;
    this.action_layer = action_layer;
    //this.walkSpeed = 25 || this.constructor.prototype.walkSpeed;
};
UNIT.prototype = {
    constructor: UNIT,
    username: '_$_USER_$_',
    X: 0,
    Y: 0,
    FPS: 40,
    speed: 5,
    //sprite_src: '',
    tileSize:[32, 48],
    tilePos:[0, 0],
    //parent_layer: '',
    //action_layer: '',
    __animate: '',
    __paths: null,
    _draw_nickname: function(){
        this.parent_layer.fillText(this.username, (this.X+GG.FIELD*0.5)-(this.parent_layer.measureText(this.username).width/2), this.Y);
    },
    SetUnit: function(){
        var unit_buffer = document.createElement("canvas"),
            ubctx = unit_buffer.getContext('2d'),
            sprite_unit = new Image();
        //this.parent_layer.font = "14px Verdana";
        this.parent_layer.font = "14px SilkscreenNormal";
        sprite_unit.addEventListener("load", function(){
            unit_buffer.width = sprite_unit.width;
            unit_buffer.height = sprite_unit.height;
            ubctx.drawImage(sprite_unit, 0, 0);
            //this is shit, use move
            this.parent_layer.drawImage(
                unit_buffer,
                this.tilePos[0], this.tilePos[1],
                this.tileSize[0], this.tileSize[1],
                this.X + 9, this.Y,
                this.tileSize[0], this.tileSize[1]);
            this._draw_nickname();
        }.bind(this));
        sprite_unit.src = this.sprite_src;
        this.Move_unit(unit_buffer);
        /*this.action_layer.addEventListener("click", function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(posX <= this.X + GG.FIELD && posY <= this.Y + GG.FIELD && posX >= this.X && posY >= this.Y){
                console.log('self click');
            }
        }.bind(this));
        this.action_layer.addEventListener("mousemove", function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(posX <= this.X + GG.FIELD && posY <= this.Y + GG.FIELD && posX >= this.X && posY >= this.Y){
                console.log('self mouseover');
            }
        }.bind(this));*/
    },
    Move_unit: function(buffer){
        var move_point = [];
        function getPossiblePaths(start, finish, map_width, fields){
            /*if(start == finish){
                move_point = start;
                return false;
            }*/
            //no paths
            if(finish.length === 0){
                return start;
            }
            var cur_fields = [],
                complete = 0;
            for(var i = 0; i < (finish.length || 1); i++){
                var _c = finish[i] || finish,
                    _top = fields[_c - map_width],
                    right = (_c + 1) % map_width == 0 ? undefined : fields[_c + 1],
                    bottom = fields[_c + map_width],
                    left = _c % map_width == 0 ? undefined : fields[_c - 1];
                /*if(isNaN(_top) && isNaN(right) && isNaN(bottom) && isNaN(left)){
                    console.log('fuck');
                }*/
                if(_top === 0){
                    fields[_c - map_width] = fields[_c] + 1;
                    cur_fields.push(_c - map_width);
                }
                else if(_top === -1){
                    complete = 1;
                    fields[_c - map_width] = fields[_c] + 1;
                    break;
                }
                if(right === 0){
                    fields[_c + 1] = fields[_c] + 1;
                    cur_fields.push(_c + 1);
                }
                else if(right === -1){
                    complete = 1;
                    fields[_c + 1] = fields[_c] + 1;
                    break;
                }
                if(bottom === 0){
                    fields[_c + map_width] = fields[_c] + 1;
                    cur_fields.push(_c + map_width);
                }
                else if(bottom === -1){
                    complete = 1;
                    fields[_c + map_width] = fields[_c] + 1;
                    break;
                }
                if(left === 0){
                    fields[_c - 1] = fields[_c] + 1;
                    cur_fields.push(_c - 1);
                }
                else if(left === -1){
                    complete = 1;
                    fields[_c - 1] = fields[_c] + 1;
                    break;
                }
            };
            if(!complete){
                getPossiblePaths(start, cur_fields, map_width, fields);
            }
            else{
                var path = getTruePath([start]);
                for(var i = 0; i < path.length; i++){
                    if(i > 0 && i < path.length - 1){
                        if(
                        (path[i - 1] + 1 == path[i] && path[i + 1] + map_width == path[i])||
                        (path[i - 1] + 1 == path[i] && path[i + 1] - map_width == path[i])||
                        (path[i - 1] - 1 == path[i] && path[i + 1] + map_width == path[i])||
                        (path[i - 1] - 1 == path[i] && path[i + 1] - map_width == path[i])||
                        (path[i - 1] + map_width == path[i] && path[i + 1] + 1 == path[i])||
                        (path[i - 1] + map_width == path[i] && path[i + 1] - 1 == path[i])||
                        (path[i - 1] - map_width == path[i] && path[i + 1] + 1 == path[i])||
                        (path[i - 1] - map_width == path[i] && path[i + 1] - 1 == path[i])
                        ){
                            move_point.push(path[i]);
                        }
                    }
                    else{
                        move_point.push(path[i]);
                    }
                }
            };
            function getTruePath(s){
                var _c = s[s.length - 1],
                    _top = fields[_c - map_width],
                    right = (_c + 1) % map_width == 0 ? undefined : fields[_c + 1],
                    bottom = fields[_c + map_width],
                    left = _c % map_width == 0 ? undefined : fields[_c - 1];
                if(_top == fields[_c] - 1 && _top != 2){
                    s.push(_c - map_width);
                    return getTruePath(s);
                }
                else if(_top == 2){
                    s.push(_c - map_width);
                    return s;
                };
                if(right == fields[_c] - 1 && right != 2){
                    s.push(_c + 1);
                    return getTruePath(s);
                }
                else if(right == 2){
                    s.push(_c + 1);
                    return s;
                };
                if(bottom == fields[_c] - 1 && bottom != 2){
                    s.push(_c + map_width);
                    return getTruePath(s);
                }
                else if(bottom == 2){
                    s.push(_c + map_width);
                    return s;
                };
                if(left == fields[_c] - 1 && left != 2){
                    s.push(_c - 1);
                    return getTruePath(s);
                }
                else if(left == 2){
                    s.push(_c - 1);
                    return s;
                };
            };
        };
        this.action_layer.addEventListener("click", function(e){
            this.__paths = GG.MB.slice();
            move_point = [];
            //get position of destination
            //browser || firefox
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY,
                destX = posX - (posX % GG.FIELD),
                destY = posY - (posY % GG.FIELD),
                dest_tile = (((destY / GG.FIELD) * GG.MS[0]) + (destX / GG.FIELD)) >> 0,
                cur_tile = (((this.Y / GG.FIELD) * GG.MS[0]) + (this.X / GG.FIELD)) >> 0;
            this.__paths[cur_tile] = -1;
            this.__paths[dest_tile] = 2;
            if(GG.MB[dest_tile] !== 1 && dest_tile != cur_tile && !GG.itbox){
                //console.log(cur_tile, dest_tile);
                getPossiblePaths(cur_tile, dest_tile, GG.MS[0], this.__paths);
                for(var i = 0; i < move_point.length; i++){
                    var row = (((move_point[i] * GG.FIELD) / (GG.MS[0] * GG.FIELD)) >> 0);
                    move_point[i] = [((move_point[i] * GG.FIELD) - (GG.MS[0] * GG.FIELD * row)), (row * GG.FIELD)];
                }
                clearInterval(this.__animate);
                var animate = setInterval(this.Move.bind(this, move_point, buffer), 1000/this.FPS);
                this.__animate = animate;
            }
        }.bind(this));
    },
    Move: function(points, _buffer){
        if(points.length <= 0){
            clearInterval(this.__animate);
        }
        else{
            this.parent_layer.clearRect(this.X-GG.FIELD, this.Y-GG.FIELD, GG.FIELD*3, GG.FIELD*2);
            //redraw nickname
            this._draw_nickname();
            //move X
            if(this.X < points[0][0]){
                //console.log('move right');
                this.tilePos[1] = this.tileSize[1] * 2;
                this.tilePos[0] == this.tileSize[0] * 2 ? this.tilePos[0] = 0 : this.tilePos[0] += this.tileSize[0];
                this.X += this.speed;
            }
            else if(this.X > points[0][0]){
                //console.log('move left');
                this.tilePos[1] = this.tileSize[1];
                this.tilePos[0] == this.tileSize[0] * 2 ? this.tilePos[0] = 0 : this.tilePos[0] += this.tileSize[0];
                this.X -= this.speed;
            }
            //move Y
            if(this.Y < points[0][1]){
                //console.log('move down');
                this.tilePos[1] = 0;
                this.tilePos[0] == this.tileSize[0] * 2 ? this.tilePos[0] = 0 : this.tilePos[0] += this.tileSize[0];
                this.Y += this.speed;
            }
            else if(this.Y > points[0][1]){
                //console.log('move up');
                this.tilePos[1] = this.tileSize[1] * 3;
                this.tilePos[0] == this.tileSize[0] * 2? this.tilePos[0] = 0 : this.tilePos[0] += this.tileSize[0];
                this.Y -= this.speed;
            }
            else if(this.X == points[0][0] && this.Y == points[0][1]){
                points.shift();
            }
            this.parent_layer.drawImage(
                            _buffer,
                            this.tilePos[0], this.tilePos[1], 
                            this.tileSize[0], this.tileSize[1],
                            this.X + 9, this.Y,
                            this.tileSize[0], this.tileSize[1]);
            }
    }
};
