function Box(parent_layer, action_layer/*, X, Y, W, H,title,  style*/){
    this.parent_layer = parent_layer;
    this.action_layer = action_layer;
    this.X = 0;
    this.Y = 0;
    this.W = 100;
    this.H = 100;
    this.show = true;
    this.active = true;
    this._title = {
        text: 'Box',
        style: {
            background: '#07f',
            color: '#000',
            font: '12px Verdana',
            border: {
                width: 0,
                color: 'transparent'
            }
        }
    };
    this.style = {
        background: '#ddd',
        color: '#000',
        font: '12px Verdana',
        padding: 5,
        shadow: {
            x: 5,
            y: 5,
            color: 'rgba(0, 0, 0, .2)'
        },
        border: {
            color: '#000', 
            width: 1
        }
    };
    this.__active = {
        background: '#ddd',
        color: '#000',
        font: '12px Verdana',
        padding: 5,
        shadow: {
            x: 5,
            y: 5,
            color: 'rgba(0, 0, 0, .5)'
        },
        border: {
            color: '#000', 
            width: 1
        }
    };
    this._close_button = {
        style: {
            padding: 2,
            margin: 4,
            background: '#ddd',
            color: '#000',
        }
    };
    this.central_place = {};
    this._widgets = [];
};
Box.prototype = {
    constructor: Box,
    //private
    itself: function(x, y){
        if(x <= this.X + this.W && y <= this.Y + this.H && x >= this.X && y >= this.Y){
            return true;
        }
        else if(x > this.X + this.W || y > this.Y + this.H || x < this.X || y < this.Y){
            return false;
        }
    },
    //private
    setStyle: function(path){
        GG.loadJSON(path, function(j){
            for(x in j){
                if(this[x]){
                    if(this[x].style){
                        //this[x].style = j[x];
                    }
                    else{
                        console.log(x, this[x], j[x]);
                        this[x] = j[x];
                    }
                }
                else if(this.style[x]){
                    console.log(x, this.style[x], j[x]);
                    this.style[x] = j[x];
                }
                /*if(/^_{1}[^_]+/.test(x)){
                    console.log("sub object");
                    console.log(x, ": ", j[x], this[x]);
                }
                else if(/^_{2}[^_]+/.test(x)){
                    console.log("sub style");
                    console.log(x, ": ", j[x], this[x]);
                }
                else{
                    console.log(x, ": ", j[x], this.style[x]);
                }*/
            };
            console.log(this.__active);
            //this.style = j;
            //this.redraw(this);
            //console.log(this, j);
        }.bind(this));
    },
    getFontWidth: function (text, font){
        this.parent_layer.font = font;
        return this.parent_layer.measureText(text).width;
    },
    //public
    setGeometry: function(x, y, w, h){
        this.X = x;
        this.Y = y;
        this.W = w;
        this.H = h;
        return this;
    },
    //public
    setTitle: function(title){
        this._title.text = title;
        this._title.style.margin = this.style.padding;
        this._title.style.padding = 5;
        this._title.H = GG.getFontHeight(this._title.text, this._title.style.font) + this._title.style.padding * 2;
        this._title.W = this.W - this._title.style.margin * 2;
        this._title.X = this.X + this._title.style.margin;
        this._title.Y = this.Y + this._title.style.margin;
        this._close_button.W = this._title.H - this._close_button.style.margin * 2;
        this._close_button.H = this._close_button.W;
        this._close_button._ox = this._title.W - (this._close_button.W + this._close_button.style.margin);
        this._close_button._oy = this._close_button.style.margin;
        this._close_button.X = this._title.X + this._close_button._ox;
        this._close_button.Y = this._title.Y + this._close_button._oy;
        this._createElementBody(this._close_button);
        this._close_button.ctx.beginPath();
        this._close_button.ctx.moveTo(this._close_button.style.padding, this._close_button.style.padding);
        this._close_button.ctx.lineTo(this._close_button.W - this._close_button.style.padding, this._close_button.H - this._close_button.style.padding);
        this._close_button.ctx.moveTo(this._close_button.W - this._close_button.style.padding, this._close_button.style.padding);
        this._close_button.ctx.lineTo(this._close_button.style.padding, this._close_button.H - this._close_button.style.padding);
        this._close_button.ctx.stroke();
        if(this.getFontWidth(this._title.text, this._title.style.font) > this._title.W - this._close_button.W){
            var normal_size = (this._title.W - this._close_button.W * 2) / this.getFontWidth(this._title.text[0], this._title.style.font) >> 0,
            normal_text = this._title.text.slice(0, normal_size);
            normal_text = normal_text.replace(/.{3}$/g, '...');
            this._title.text = normal_text;
        }
        this._createElementBody(this._title); 
        return this;
    },
    //public
    setBox: function(){
        this.removeBrowserKeyBindings();
        if(!this._title.body){this.setTitle('Box')};
        this._createElementBody(this);
        this.central_place = {
            W: this.W - this.style.padding * 2,
            H: this.H - (this.style.padding * 3 + this._title.H),
            X: this.style.padding,
            Y: this.style.padding * 2 + this._title.H/* + this._title.style.padding * 2*/
        };
        /*this.central_place.style = {};
        this.central_place.style.border = {};
        this.central_place.style.border.width = 1;
        this.central_place.style.border.color = '#000';*/
        this._createElementBody(this.central_place);
        this.Draw();
        this.toggleStyle(this);
        this.Drag();
        //this.textEdit();
        this.on(this._close_button, "click", function(e){
            //console.log(e);
            this.close();
        });
        this.on(this, "mousedown", function(){
            GG.itbox = true;
        });
        for(var i = 0; i < this._widgets.length; i++){
            var cur_w = this._widgets[i];
            if(cur_w.callback){
                this.on(cur_w, "click", cur_w.callback);
            }
            if(cur_w.widgetType == "line_edit"){
                this.on(cur_w, "click", cur_w.textEdit.bind(this, cur_w));
            }
            //console.log(cur_w, cur_w.active);
            if(cur_w.active !== undefined){
                //this.on(cur_w, "click", function(){this.toggleStyle(cur_w)});
                this.toggleStyle(cur_w);
            }
        };
    },
    //public
    close: function(){
        this.clear();
        this.show = false;
        this.W = 0;
        this.H = 0;
        this.X = 0;
        this.Y = 0;
        for(var i = 0; i < this._widgets.length; i++){
            this._widgets[i].X = 0;
            this._widgets[i].Y = 0;
            this._widgets[i].H = 0;
            this._widgets[i].W = 0;
        }
    },
    //private
    clear: function(){
        this.parent_layer.clearRect(0, 0, this.action_layer.width, this.action_layer.height);
    },
    //private
    Draw: function(){
        if(this.show){
            this._title.X = this.X + this._title.style.margin;
            this._title.Y = this.Y + this._title.style.margin;
            this._close_button.X = this._title.X + this._title.W - (this._close_button.W + this._close_button.style.margin / 2);
            this._close_button.Y = this._title.Y + (this._close_button.style.margin / 2);
            this._title.ctx.drawImage(this._close_button.body, this._close_button._ox, this._close_button._oy);
            this.ctx.drawImage(this._title.body, this._title.style.margin, this._title.style.margin);
            for(var i = 0; i < this._widgets.length; i++){
                this._widgets[i].X = this.X + this._widgets[i]._ox;
                this._widgets[i].Y = this.Y + this._widgets[i]._oy;
                this.central_place.ctx.drawImage(this._widgets[i].body, this._widgets[i]._ox - this.central_place.X, this._widgets[i]._oy - this.central_place.Y);
            }
            this.ctx.drawImage(this.central_place.body, this.central_place.X, this.central_place.Y);
            this.parent_layer.shadowOffsetX = this.style.shadow.x;
            this.parent_layer.shadowOffsetY = this.style.shadow.y;
            this.parent_layer.shadowColor = this.style.shadow.color;
            this.parent_layer.drawImage(this.body, this.X, this.Y);
        }
    },
    //private
    toggleStyle: function(obj){
        console.log(obj.style, obj.__active);
        var prev_state;
            //_m = 0;
        if(obj.active){
            prev_state = obj.style;
            obj.style = obj.__active;
            this.redraw(obj, true);
        }
        //remove double listener!
        this.action_layer.addEventListener("mousedown", function(e){
            console.log('toggle');
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(this.itself.call(obj, posX, posY) && !obj.active){
                //this._title.text = 'active';
                //this._createElementBody(this._title, true);
                prev_state = obj.style;
                obj.style = obj.__active;
                this.redraw(obj, true);
                obj.active = true;
            }
            else if(
            ((!this.itself.call(obj, posX, posY) && obj.constructor.name != "Checkbox") || 
            (this.itself.call(obj, posX, posY) && obj.constructor.name == "Checkbox")) && obj.active ){
                obj.style = prev_state;
                this.redraw(obj, true);
                obj.active = false;
            }
        }.bind(this));
    },
    //private
    Drag: function(){
        var ofsX, ofsY, prev_style;
        var mov = function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            this.X = posX - ofsX;
            this.Y = posY - ofsY;
            this.clear();
            this.Draw();
        }.bind(this);
        this.action_layer.addEventListener("mousedown", function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(this.itself.call(this._title, posX, posY)){
                ofsX = posX - this.X;
                ofsY = posY - this.Y
                this.action_layer.addEventListener("mousemove", mov);
            }
        }.bind(this));
        this.action_layer.addEventListener("mouseup", function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(!this.itself(posX, posY)){GG.itbox = false;}
            this.action_layer.removeEventListener("mousemove", mov);
        }.bind(this));
    },
    removeBrowserKeyBindings: function(){
        document.addEventListener("contextmenu", function(e){
            e.preventDefault();
        });
        document.addEventListener("keydown", function(e){
            //console.log(e.which, e.keyCode, e.charCode);
            //console.log(e);
            if(e.which == 9 || e.which == 8 || e.altKey /*|| e.ctrlKey*/){
                e.preventDefault();
            }
        });
    },
    redraw: function(obj){
        this._createElementBody(obj, true);
        this.clear();
        this.Draw();
    },
    textEdit: function(obj){
        obj.cursor_show = true;
        this.redraw(obj);
        var gfw = this.getFontWidth.bind(this),
            font = obj.style.font,
            padding = obj.style.padding,
            printChar, moveCursor, removeChar, largText, removeInternalListener;
        //maybe move it method to Box.prototype?
        largText = function(){
            return !!(gfw(obj.text, font) > obj.W - padding * 2 - gfw(obj.text[obj.text.length - 1], font));
        }
        printChar = function(e){
            var ch = String.fromCharCode(e.which || e.keyCode);
            //detect char keys, filtering backspace for Opera
            if(e.which && e.keyCode != 8){
                if(largText()){
                    obj.txt_offset_x -= gfw(obj.text[obj.text.length - 1], font);
                };
                obj.text = obj.text.substr(0, obj.cursor_position) + ch + obj.text.substr(obj.cursor_position);
                obj.cursor_position++;
                this.redraw(obj); 
            }
        }.bind(this);
        moveCursor = function(e){
            //left key
            if(e.keyCode == 37 && obj.cursor_position > 0){
                obj.cursor_position--;
                if(largText() && obj.txt_offset_x < 0 && obj.txt_offset_x_px <= obj.style.padding * 2){
                    obj.txt_offset_x += gfw(obj.text[0], font);
                };
                this.redraw(obj);
            }
            //right key
            if(e.keyCode == 39 && obj.cursor_position < obj.text.length){
                obj.cursor_position++;
                if(largText() && obj.txt_offset_x <= 0 && obj.txt_offset_x_px >= obj.W - obj.style.padding * 2){
                    obj.txt_offset_x -= gfw(obj.text[obj.text.length - 1], font);
                };
                this.redraw(obj);
            }
            //enter key
            if(e.keyCode == 13){
                if(obj.callback){
                    obj.callback();
                }
            }
        }.bind(this);
        removeChar = function(e){
            //console.log(e);
            var keyCode = e.which || e.keyCode;
            //delete key
            if(keyCode == 46){
                if(obj.cursor_position < obj.text.length){
                    obj.text = obj.text.substr(0, obj.cursor_position) + obj.text.substr(obj.cursor_position + 1, obj.text.length - 1);
                    this.redraw(obj);
                }
            }
            //backspace key
            if(keyCode == 8){
                if(obj.cursor_position > 0){
                    obj.text = obj.text.substr(0, obj.cursor_position - 1) + obj.text.substr(obj.cursor_position, obj.text.length - 1);
                    obj.cursor_position--;
                    if(largText()){
                        obj.txt_offset_x += gfw(obj.text[obj.text.length - 1], font);
                    };
                    this.redraw(obj);
                }
            }
        }.bind(this);
        removeInternalListener = function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(!this.itself.call(obj, posX, posY)){
                obj.cursor_show = false;
                obj.cursor_position = obj.text.length;
                this.redraw(obj);
            }
            if(this.itself(posX, posY)){
                document.removeEventListener("keypress", printChar);
                document.removeEventListener("keydown", removeChar);
                document.removeEventListener("keydown", moveCursor);
            };
            this.action_layer.removeEventListener("click", removeInternalListener);
        }.bind(this);
        document.addEventListener("keypress", printChar); 
        document.addEventListener("keydown", removeChar); 
        document.addEventListener("keydown", moveCursor);
        this.action_layer.addEventListener("click", removeInternalListener);
    },
    //publick
    on: function(obj, evnt, callback){
        this.action_layer.addEventListener(evnt, function(e){
            var posX = e.offsetX || e.layerX,
                posY = e.offsetY || e.layerY;
            if(this.itself.call(obj, posX, posY)){
                callback.call(this, e);
            };
        }.bind(this));
    },
    _createElementBody: function(el, redraw){
        //console.log(el);
        if(el.body && el.ctx && !redraw){
            //console.log('i have body');
            return el;
        }
        else{
            //console.log('i have no body');
            var _t = document.createElement('canvas'),
                _tctx = _t.getContext('2d');
            _t.width = el.W;
            _t.height = el.H;
            if(el.style){
                _tctx.fillStyle = el.style.background;
                _tctx.fillRect(0, 0, el.W, el.H);
                if(el.style.border){
                    _tctx.lineWidth = el.style.border.width;
                    _tctx.strokeStyle = el.style.border.color;
                    _tctx.strokeRect(0, 0, el.W, el.H);
                }
                if(el.text){
                    var _tx = el.txt_offset_x || el.style.padding,
                        _ty = el.txt_offset_y || el.style.padding ;
                    _tctx.fillStyle = el.style.color;
                    _tctx.font = el.style.font;
                    _tctx.textBaseline = "top";
                    if(el.wrap_text){
                        for(var i = 0, h = 0; i < el.text.length; i++, h += (el.H/el.text.length)){
                            _tctx.fillText(el.text[i], _tx, h + _ty);
                        }
                    }
                    else{
                        _tctx.fillText(el.text, _tx, _ty);
                    }
                }
                if(el.cursor_position >= 0 && el.cursor_show){
                    el.txt_offset_x_px = el.txt_offset_x + this.getFontWidth(el.text.slice(0, el.cursor_position), el.style.font);
                    if(el.txt_offset_x == 0){
                        el.txt_offset_x_px += el.style.padding;
                    }
                    if(el.txt_offset_x > 0){
                        el.txt_offset_x = 0;
                    }
                    //console.log('cursor_position: ', el.cursor_position, "text_offset_x: ", el.txt_offset_x);
                    //console.log('pos: ', el.txt_offset_x_px);
                    _tctx.fillStyle = el.style.color;
                    _tctx.fillRect(el.txt_offset_x_px, el.style.padding, 1,  GG.getFontHeight(el.style.font, el.text))
                }
            }
            el.body = _t;
            el.ctx = _tctx;
            return el;
        }
    },
    pushWidget: function(widget){
        widget.text = widget.text || '';
        widget.style = widget.style || this.style;
        callback = widget.callback || function(){};
        widget.W = widget.W || this.getFontWidth(widget.text, widget.style.font) + widget.style.padding * 2;
        widget.H = widget.H || GG.getFontHeight(widget.text, widget.style.font) + widget.style.padding * 2;
        widget._ox = widget.X || this.style.padding;
        widget._oy = widget.Y || this._title.H + this.style.padding * 2;
        if(widget.wrap_text){
            var words = widget.text.split(' '),
                line = "",
                res = [];
            for(var i = 0; i < words.length; i++){
                var width = this.getFontWidth(line + words[i] + " ", widget.style.font);
                if(width > this.W - this.style.padding * 2){
                    res.push(line);
                    line = words[i] + " ";
                }
                else{
                    line += words[i] + " ";
                }
            }
            widget.H = widget.H * res.length;
            widget.text = res;
        }
        this._createElementBody(widget)
        this._widgets.push(widget);
        return widget;
    }
};
Box.Button = (function(){
    function Button(text, callback){
        this.text = text || "Button";
        this.callback = callback || function(){};
        this.style = {
            background: '#ddd',
            color: '#000',
            font: '12px Verdana',
            padding: 5,
            margin: 0,
            border: {
                width: 1,
                color: '#000',
            }
        }
    };
    Button.prototype = {
        constructor: Button,
        setGeometry: Box.prototype.setGeometry,
        widgetType: 'button'
    };
    return Button;
})();
Box.Label = (function(){
    function Label(text){
        this.text = text;
        this.style = {
            background: '#ddd',
            color: '#000',
            font: '12px Verdana',
            padding: 2,
            margin: 0
            /*border: {
                color: 'transparent', 
                width: 0
            }*/
        };
    };
    Label.prototype = {
        constructor: Label,
        setGeometry: Box.prototype.setGeometry,
        widgetType: 'label',
        wrap_text: true
    };
    return Label;
})();
Box.LineEdit = (function(){
    function LineEdit(text){
        this.text = text || '';
        this.cursor_position = this.text.length;
        this.cursor_show = false;
        //this.callback = function(){console.log(this.text)};
        this.txt_offset_x =  0;
        this.txt_offset_y =  0;
        this.txt_offset_x_px = 0;
        this.txt_offset_y_px = 0;
        this.active = false;
        this.style = {
            background: '#ddd',
            color: '#000',
            font: '15px Verdana',
            padding: 5,
            margin: 0,
            border: {
                color: '#000', 
                width: 1
            }
        };
        this.__active = {
            background: '#ddd',
            color: '#000',
            font: '15px Verdana',
            padding: 5,
            margin: 0,
            border: {
                color: '#000', 
                width: 5
            }
        };
    };
    LineEdit.prototype = {
        constructor: LineEdit,
        setGeometry: Box.prototype.setGeometry,
        textEdit: Box.prototype.textEdit,
        widgetType: 'line_edit'
    };
    return LineEdit;
})();
Box.Checkbox = (function(){
    function Checkbox(st){
        this.active = st||false;
        this.style = {
            background: '#ddd',
            //color: '#000',
            //font: '15px Verdana',
            padding: 5,
            margin: 0,
            border: {
                color: '#000', 
                width: 1
            }
        };
        this.__active = {
            background: '#ddd',
            //color: '#000',
            //font: '15px Verdana',
            padding: 5,
            margin: 0,
            border: {
                color: '#000', 
                width: 10
            }
        };
    };
    Checkbox.prototype = {
        constructor: Checkbox,
        setGeometry: Box.prototype.setGeometry,
        widgetType: 'checkbox'
    };
    return Checkbox;
})();
function AreaEdit(text){
};
