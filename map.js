var MAP = function(MapURL){
    this.MapURL = MapURL;
    this.MapSRC = null;
    this.MapFields = null;
    this.MapBarriers = null;
    this.MapSize = [];
}
MAP.prototype = {
    constructor: MAP,
    //MapURL: '',
    //MapSRC: '',
    //MapFields: '',
    //MapBarriers: '',
    //MapReady: '',
    SetMap: function(){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET', this.MapURL, true);
        xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState == 4){
                if(xmlhttp.status == 200){
                    var json_map = JSON.parse(xmlhttp.responseText);
                    this.MapSRC = json_map.MapSRC;
                    this.MapFields = json_map.MapFields;
                    this.MapBarriers = json_map.MapBarriers;
                    this.MapSize = [json_map.MapWidth, json_map.MapHeight];
                    GG.MS = this.MapSize;
                    GG.MF = this.MapFields;
                    GG.MB = this.MapBarriers;
                    var tile_map = new Image();
                    tile_map.src = this.MapSRC;
                    tile_map.addEventListener("load", function(){
                        for(var i = 0, j = 0; j < this.MapFields.length; i += GG.FIELD, j ++){
                            //math floor == x >> 0
                            var cur_str = ((i / GG.WIDTH) >> 0) * GG.FIELD,
                                cur_row = i - (cur_str * (GG.WIDTH / GG.FIELD)),
                                cur_map_x = this.MapFields[j] * GG.FIELD - ((this.MapFields[j] * GG.FIELD) > 100 ? 150 : 0),
                                cur_map_y = ((this.MapFields[j] / 3) >> 0) * GG.FIELD;
                                GG.BGCTX.drawImage(tile_map, cur_map_x, cur_map_y, GG.FIELD, GG.FIELD, cur_row, cur_str, GG.FIELD, GG.FIELD);
                        }
                    }.bind(this));
                }
            }
        }.bind(this);
        xmlhttp.send(null);
    }
}
