document.addEventListener("DOMContentLoaded", function(){
    var background = document.getElementById("background"),
        CTX = background.getContext("2d"),
        mouse_layer = document.getElementById("mousemove"),
        mouse_ctx = mouse_layer.getContext("2d"),
        foreground = document.getElementById("foreground"),
        foreground_ctx = foreground.getContext("2d");
    (function(){
        var _GG = {
            WIDTH: 800,
            HEIGHT: 600,
            FIELD: 50,
            BGCTX: CTX,
            MF: null,
            MB: null,
            itbox: false,
            getFontHeight: function (text, font){
                var p = document.createElement("span");
                p.appendChild(document.createTextNode(text));
                document.body.appendChild(p);
                p.style.cssText = "font: " + font + "; white-space: nowrap; display: inline;";
                var height = p.offsetHeight;
                document.body.removeChild(p);
                return height;
            },
            loadJSON: function(path, callback){
                var xhr = new XMLHttpRequest();
                xhr.open('GET', path, true);
                xhr.onreadystatechange = function(){
                    if(xhr.readyState == 4){
                        if(xhr.status == 200){
                            callback(JSON.parse(xhr.responseText));
                        }
                    }
                }
                xhr.send(null);
            },
            /*getFontWidth: function (text, font){
                var _t = document.createElement('canvas').getContext('2d');
                _t.font = font;
                return _t.measureText(text).width;
            },*/
            itself: function(x, y){
                if(x <= this.X + this.W && y <= this.Y + this.H && x >= this.X && y >= this.Y){
                    GG.itbox = true;
                    return true;
                }
                else if(x > this.X + this.W || y > this.Y + this.H || x < this.X || y < this.Y){
                    GG.itbox = false;
                    return false;
                }
            },
        }
        window.GG = _GG;
    })();
    background.width = GG.WIDTH;
    background.height = GG.HEIGHT;
    foreground.width = GG.WIDTH;
    foreground.height = GG.HEIGHT;
    mouse_layer.width = GG.WIDTH;
    mouse_layer.height = GG.HEIGHT;
    background.style.zIndex = 0;
    foreground.style.zIndex = 1;
    mouse_layer.style.zIndex = 999;
    function zoom(){
        var zoomX = (((window.screen.width - GG.WIDTH) / 50) >> 0) * 50,
            zoomY = (((window.screen.height - GG.HEIGHT) / 50) >> 0) * 50;
        background.style.width = GG.WIDTH + zoomX + 'px';
        background.style.height = GG.HEIGHT + zoomY + 'px';
        foreground.style.width = GG.WIDHT + zoomX + 'px';
        foreground.style.height = GG.HEIGHT + zoomY + 'px';
        mouse_layer.style.width = GG.WIDTH + zoomX + 'px';
        mouse_layer.style.height = GG.HEIGHT + zoomY + 'px';
        console.log(zoomX, zoomY);
    }
    //zoom();
    /*GG.loadJSON('Box_style.json', function(data){
        console.log(data);
    });*/
    var cur_unit = new UNIT(300, 50, 'unit_sprite2.png', foreground_ctx, mouse_layer, "sweetrenard");
    cur_unit.SetUnit();
    var cur_map = new MAP('map.json');
    cur_map.SetMap();
    var test_window = new Box(mouse_ctx, mouse_layer); 
    //test_window.setStyle('Box_style.json');
    test_window.setGeometry(200, 100, 300, 200);
    test_window.setTitle("Test Window");
    var tb2 = new Box.Button('close', function(){test_window.close()});
    tb2.setGeometry(100, 160, 90);
    var tl = new Box.Label("A change in this context is considered as all editing you have made while in insert mode, or a single editing command in normal or command-line mode");
    var tle = new Box.LineEdit("placeholder");
    tle.setGeometry(0, 100, 190, 30);
    var tb = new Box.Button('ok', function(){console.log(tle.text)});
    tb.setGeometry(0, 160, 90);
    var cb = new Box.Checkbox();
    cb.setGeometry(0, 140, 15, 15);
    //console.log(cb.constructor.name);
    //tl.setGeometry(100, 100);
    //console.log(tl);
    test_window.pushWidget(tl);
    test_window.pushWidget(tb);
    test_window.pushWidget(tb2);
    test_window.pushWidget(tle);
    test_window.pushWidget(cb);
    //console.log(tb, tb2.widgetType);
    test_window.setBox();
    /*var test_window2 = new Box(mouse_ctx, mouse_layer);
    test_window2.setGeometry(100, 100, 150, 150);
    test_window2.style.background = '#f00';
    test_window2.title.style.background = '#ffe';
    test_window2.setBox();
    var test_window3 = new Box(mouse_ctx, mouse_layer);
    test_window3.setBox();
    console.log(test_window, test_window2, test_window3);*/
});
